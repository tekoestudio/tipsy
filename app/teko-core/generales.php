<?php

//Redireccionar
function teko_redirect($url, $codigo = 303){
    header('Location: ' . $url, true, $codigo);
    die();
}

//Respuesta para Ajax en JSON
function teko_json($arr){
    header('Content-Type: application/json');
    echo json_encode($arr);
}