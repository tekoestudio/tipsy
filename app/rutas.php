<?php

//Header de acceso
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: origin, content-type, accept, clavesupersecretadetekoestudio");

$rutas->home(function($View){
    global $titulo;
    $titulo = 'Inicio';
    $View->display('index');
});

$rutas->get('api/usuario',function(){
    global $tekodb;
    $headers = getallheaders();
    if(array_key_exists('ClaveSuperSecretaDeTekoEstudio', $headers) && $headers['ClaveSuperSecretaDeTekoEstudio'] == 'Holi :3'){
        teko_json($tekodb->get_results("SELECT * FROM customer LIMIT 50"));
    } else {
        
    }
});

$rutas->otherwise(function($View){
    global $titulo;
    $titulo = '404';
    $View->display('404');
});